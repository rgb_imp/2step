// массив активных задач
var dataToDo = (localStorage.getItem('todoList'))
  ? JSON.parse(localStorage.getItem('todoList'))
  : [];


//массив сделанных задач
var dataDone = (localStorage.getItem('doneList'))
? JSON.parse(localStorage.getItem('doneList'))
: [];


// синхронизация localStorage и Массива JS
function storageSync() {
  localStorage.setItem('todoList', JSON.stringify(dataToDo));
  localStorage.setItem('doneList', JSON.stringify(dataDone));
}


// рисуем начальный список
renderTodoList();

//рисуем список сделанных
renderDoneList();

// Добавить обработку создания новой задачи
document.getElementById('btn-create').addEventListener('click', 
  function () {
    var value = document.getElementById('input-create').value;
    if (value) {
        addNewItemToDOM(value);
        document.getElementById('input-create').value = '';

        dataToDo.push(value);  

        storageSync();
      } 
  }
);


//функция отрисовки списка новых задач
function renderTodoList() {
  if (!dataToDo.length) return;

  for (var i = 0; i < dataToDo.length; i++) {
    var value = dataToDo[i];
    addNewItemToDOM(value);
  }
}

//функция добавления отрисовки новой задачи в разметку
function addNewItemToDOM(text) {
  var listToDo = document.getElementById('todo-list');

  var newItem = document.createElement('li'); 
  newItem.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
      <div class="item-btn item-btn-move btn-floating btn-small waves-effect waves-light green">v</div>  
      <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
          
    </span>
  </div>`;
  newItem.innerHTML = listItemView;

  // добавим слушатель для удаления
  var buttonDelete = newItem.getElementsByClassName('item-btn-del')[0];
  buttonDelete.addEventListener('click', removeItem);

  // слушатель для переноса
  var buttonMove = newItem.getElementsByClassName('item-btn-move')[0];
  buttonMove.addEventListener('click', moveItem);

  listToDo.appendChild(newItem);
}

//функция удаления задачи из списка активных
function removeItem(e) {
  var item = this.parentNode.parentNode.parentNode;
  var list = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;

  dataToDo.splice(dataToDo.indexOf(value), 1);
  
  list.removeChild(item);
  storageSync();
}

//функция переноса задачи из списка активных в список сделанных
function moveItem() {
  var item = this.parentNode.parentNode.parentNode;
  var listToDo = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;
  dataToDo.splice(dataToDo.indexOf(value), 1);
  listToDo.removeChild(item);
  dataDone.push(value);
  addDoneItemToDOM(value);
  storageSync();
}



//функция добавления отрисовки новой задачи в разметку
function addDoneItemToDOM(text) {
  var listDone = document.getElementById('done-list');

  var doneItem = document.createElement('li'); 
  doneItem.classList = 'collection-item';
  var listDoneItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
  </div>`;
  doneItem.innerHTML = listDoneItemView;
  listDone.appendChild(doneItem);
}

//функция отрисовки списка сделанных задач
function renderDoneList() {
  if (!dataDone.length) return;

  for (var i = 0; i < dataDone.length; i++) {
    var value = dataDone[i];
    addDoneItemToDOM(value);
  }
}


