var allKeys = [1, 2, 3, '+', 4, 5, 6, '-', 7, 8, 9, '*', 'C', 0, '=', '/'];

//отрисовка кнопок
renderKeys();

//добавление разметки для клавиши рабочего поля в DOM
function addKey(value) {
    var keys = document.getElementsByClassName('keys')[0];
    var key = document.createElement('div');
    key.classList.add('key', 'key-click');
    var keyView = `${value}`;
    key.innerHTML=keyView;
    keys.appendChild(key);

    //слушатель для нажатия
    key.addEventListener('click', showValue);
    
    

}

//добавление клавиши в рабочее поле
function renderKeys() {
    for (var i=0; i<allKeys.length; i++) {
        var value = allKeys[i];
        addKey(value);
    }
}

//передача значения в поле вывода и обработка
function showValue() {
    var value = this.innerText;
    //console.log(value);
    var valueOutput = document.getElementsByClassName('output')[0];
    //console.log(valueOutput);
    var previousValue = valueOutput.innerHTML;
    //console.log(previousValue+value);
    var operations=['+','-','*','/'];
    if (previousValue=='0' || operations.includes(previousValue)) {
        valueOutput.innerHTML=value;
       
    } else {
            switch (value) {
                case 'C': 
                    valueOutput.innerHTML=0; 
                    break;
                case '+': 
                case '-':
                case '*':
                case '/':
                    
                    switch (value) {
                        case '+': operation=sum.bind(null,previousValue); break;
                        case '-': operation=subs.bind(null,previousValue); break;
                        case '*': operation=mpl.bind(null,previousValue); break;
                        case '/': operation=dvd.bind(null,previousValue); break;
                    }
                    previousValue=0;
                    valueOutput.innerHTML=value;
                    break;
                case '=':
                    
                    valueOutput.innerHTML=operation(previousValue);
                    previousValue=0;
                    operation='=';
                    break;
                default : 
                    valueOutput.innerHTML=previousValue+value; 
                    break;
            }

        }

}

let operation;

function sum(a,b) { return Number(a)+Number(b);}
function subs(a,b) { return Number(a)-Number(b);}
function mpl(a,b) { return Number(a)*Number(b);}
function dvd(a,b) { return Number(a)/Number(b);}


